const $ = require('jquery');
const Waypoints = require('./components/waypoints');
const Pano = require('./components/pano');
const VimeoGA = require('./components/vimeo-ga');

$(document).ready(function() {

    var $win = $(window),
        $body = $('body'),
        $fixed = $('.fix-pos'),
        $endFixed = $('.end-fixed'),
        $left = $('.left'),

        resizeBG = function() {

            var $img = $('.section--1a .fixed-img img'),
                aspectRatio = $img.width() / $img.height(),
                $winRatio = $win.width() / $win.height();

            if ($win.width() < $win.height()) {
                $('.fixed-img img').each(function(i) {
                    $(this).removeClass().addClass('bgheight');
                });
            } else {
                $('.fixed-img img').each(function(i) {
                    $(this).removeClass().addClass('bgwidth');
                });
            }
        }

    resizeBG();

    window.addEventListener('resize', function() {
        resizeBG();
    });

    var pano = $('.panorama').pano({
        img: 'Content/images/studio.jpg'
    });

    ga('send', 'event', 'Section Scroll', 'Scroll', '1 - Welcome');

    var waypoint2 = new Waypoint({
        element: document.getElementById('better-ways'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '2 - Better Ways');
            }
        }
    });

    var waypoint3 = new Waypoint({
        element: document.getElementById('fix'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '3 - Paint by Number');
                $left.addClass('fixed');
            } else {
                $left.removeClass('fixed');
            }
        }
    });

    var waypointRemoveFix = new Waypoint({
        element: document.getElementById('unfix'),
        handler: function(direction) {
            if (direction === 'down') {
                $left.removeClass('fixed');
            } else {
                $left.addClass('fixed');
            }
        }
    });

    var waypoint4 = new Waypoint({
        element: document.getElementById('more-hands-on'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '4 - More Hands-On');
            }
        }
    });

    var waypoint5 = new Waypoint({
        element: document.getElementById('two-sides'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '5 - Two Sides');
            }
        }
    });

    var waypoint6 = new Waypoint({
        element: document.getElementById('see-for-yourself'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '6 - See for Yourself');
            }
        }
    });

    var waypoint7 = new Waypoint({
        element: document.getElementById('another-side'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '7 - Another Side');
            }
        }
    });

    var waypoint8 = new Waypoint({
        element: document.getElementById('vr-fingerpainting'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '8 - VR Fingerpainting');
            }
        }
    });

    var waypoint9 = new Waypoint({
        element: document.getElementById('fingerpaint-fingerpaint'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '9 - Fingerpaint Fingerpaint');
            }
        }
    });

    var waypoint10 = new Waypoint({
        element: document.getElementById('bottom'),
        handler: function(direction) {
            if (direction === 'down') {
                ga('send', 'event', 'Section Scroll', 'Scroll', '10 - Bottom');
            }
        },
        offset: 'bottom-in-view'
    });

});
